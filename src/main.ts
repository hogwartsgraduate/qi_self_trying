import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import "@/styles/common.less"
import uiPlugin from "@/plugin/ui.registry"
import "@/styles/var.less"
import "@/styles/theme.css"
import baseComponents from "@/plugin/baseComponet.registry"
import { createPinia } from "pinia"
const pinia = createPinia()
createApp(App)
  .use(pinia)
  .use(router)
  .use(uiPlugin)
  .use(baseComponents)
  .mount("#app")
