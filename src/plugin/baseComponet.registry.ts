import mainHeader from "@/components/mainHeader.vue"
import icon from "@/components/iconFont/index"
import { App } from "vue"
export default {
  install(app: App): void {
    Object.keys(icon).forEach((key) => {
      app.component(key, icon[key])
    })
    app.component("mainHeader", mainHeader)
  },
}
