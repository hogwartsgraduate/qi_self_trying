import { Button, Rate, message } from "ant-design-vue"
import { App } from "vue"
const install = (app: App) => {
  app.use(Button)
  app.use(Rate)
  app.config.globalProperties.$message = message
}

export default {
  install,
}
