const baseRouterData = [
  {
    path: "/artical",
    component: () => import("views/artical/artical.vue"),
    meta: {
      title: "文章",
      nav: true,
    },
  },
  {
    path: "/filed",
    component: () => import("views/filed/filed.vue"),
    meta: {
      title: "归档",
      nav: true,
    },
  },
  {
    path: "/knowledge",
    component: () => import("views/knowledge/knowledge.vue"),
    meta: {
      title: "知识小册",
      nav: true,
    },
  },
  {
    path: "/page/:id",
    component: () => import("views/page/page.vue"),
    meta: {
      nav: false,
    },
  },
]
export default baseRouterData
