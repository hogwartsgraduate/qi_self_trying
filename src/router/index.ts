import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router"
import baseRouterData from "./baseRouter"
const routes: Array<RouteRecordRaw> = [...baseRouterData]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
