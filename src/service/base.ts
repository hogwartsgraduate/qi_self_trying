//公共接口
import http from "@/utils/httpTool"
import { Params } from "@/utils/httpTool"

export const getNavPage = (params: Params = {}) => http.get("/api/page", params)
