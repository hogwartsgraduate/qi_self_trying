// pinia官网，what is Pinia的Basic example找到
import { defineStore } from "pinia"
import { base } from "@/service"

const useStore = defineStore("baseStore", {
  state: () => ({
    navList: [],
  }),
  actions: {
    async getNavData() {
      const { data } = await base.getNavPage()
      this.$patch({
        navList: data[0].filter((item: any) => item.id),
      })
    },
  },
})
export default useStore
