const statusCodeMap: any = {
  "400": "参数错误",
  "401": "您当前身份不明确，或者登录态过期",
  "403": "您暂无权限",
  "500": "服务器异常",
}
export default statusCodeMap
