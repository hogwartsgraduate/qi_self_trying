import axios from "axios"
import { message } from "ant-design-vue"
import statusCodeMap from "./codeMap"
export interface PageNationParams {
  page?: number
  pageSize?: number
}
export interface Params extends PageNationParams {
  [propertyName: string]: string | number | boolean | undefined
}

const httpTool = axios.create({
  timeout: 10000,
  baseURL: process.env.VUE_APP_BASEURL, //区分环境  开发环境接口，生产环境接口
})
httpTool.interceptors.response.use(
  (response) => {
    if (response.data?.success) {
      return response.data
    }
    message.error(
      response.data?.msg || statusCodeMap[response.data?.statusCode || 500]
    )
    return response.data
  },
  (error) => {
    if (error.code === "ECONNABORTED") {
      message.error("您当前网络状态不好")
    } else {
      message.error(
        error.response?.statusText ||
          statusCodeMap[error.response.data?.statusCode || 400]
      )
    }
    return Promise.reject(error)
  }
)

export default {
  ...httpTool,
  get(url: string, params: Params) {
    console.log(url, params)
    return httpTool.get(url, {
      params,
    })
  },
  post() {
    console.log("post")
  },
}
