/* eslint-disable */
const path = require("path")

module.exports = {
  devServer: {
    proxy:{   //开发环境才会走这个代理
      '/api': {//vue cli官网配置里的devServer.proxy
        target: 'https://creationapi.shbwyz.com',
        ws: true, //支持WebSocket
        changeOrigin: true//支持跨域
      },
    }
  },
  chainWebpack: (config) => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"]
    types.forEach((type) =>
      addStyleResource(config.module.rule("less").oneOf(type))
    )
    config //设置页面的默认标题 在vue-cli官网，webpack相关里的修改插件
      .plugin('html')
      .tap(args => {
        args[0].title = 'welcome to Hogwarts'
        return args
      })
  },
  configureWebpack: {//在vue-cli官网，webpack相关里
    resolve: {//webpack官网配置里的解析
      alias: {
        'views': path.join(__dirname, 'src/views'),
      },
    },
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
        }
      }
    }
  },
}

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/styles/var.less")],
    })
}
